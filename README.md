# Conjuntos, Aplicaciones y funciones 

```plantuml

@startmindmap
title Conjuntos, Aplicaciones y funciones
center footer Herrera Mendoza Juan Antonio
<style>
mindmapDiagram {
  .tema1 {
    BackgroundColor #FA7792
  }
  .tema2{
      BackgroundColor #FA77B4
  }
  .tema3 {
    BackgroundColor #FA77CC
  }
  .tema4 {
    BackgroundColor #FA77FA
  }
  .tema5 {
    BackgroundColor #D377FA
  }
  .tema6 {
    BackgroundColor #A577FA
  }
  .tema7 {
    BackgroundColor #7792FA
  }
  .tema8 {
    BackgroundColor #77B9FA
  }
  .tema9 {
    BackgroundColor #77DDFA
  }
  .tema10 {
    BackgroundColor #77FAF5
  }
}
</style>

* Conjuntos, Aplicaciones y funciones 
** Matematicas <<tema1>>
***_ Tiene como tareas
**** Dar solucion a problemas <<tema1>>
**** Reflexionar <<tema1>>
*****_ Acerca de
****** Razonar <<tema1>>
*******_ Cercana a
******** Filosofia <<tema1>>
** Terminos abstractos <<tema2>>
***_ Intuidos por el
**** Observador <<tema2>>
*****_ Los cuales son
****** Conjunto <<tema2>>
****** Elemento <<tema2>>
** Teoria de conjuntos <<tema3>>
*** Inclusion de conjuntos <<tema4>>
****_ Consiste en
***** Un conjunto <<tema4>>
******_ Esta incluido en otro cuando
******* Los elementos del primero <<tema4>>
********_ Estan 
********* En el segundo conjunto <<tema4>>
*** Operaciones <<tema5>>
**** Interseccion <<tema5>>
*****_ Consiste en 
****** Elementos <<tema5>>
*******_ Los cuales pertenecen
******** A los conjuntos evaluados <<tema5>>
**** Union <<tema5>>
*****_ Consiste en
****** Elementos de 2 o mas conjuntos <<tema5>>
*******_ Dando como resultado 
******** Un nuevo conjunto <<tema5>>
**** Complementacion <<tema5>>
*****_ Consiste en 
****** Elementos de un conjunto <<tema5>>
*******_ Que no pertenecen
******** En un conjunto dado <<tema5>>
**** Diferencia <<tema5>>
*****_ Consiste en
****** Elementos del primer conjunto <<tema5>>
*******_ Los cuales no estan
******** En el segundo conjunto <<tema5>>
*** Conjuntos Especiales <<tema6>>
**** Conjunto Universal <<tema6>>
*****_ Es un
****** Conjunto <<tema6>>
*******_ Que contiene 
******** Todos los elementos <<tema6>>
**** Conjunto Vacio <<tema6>>
*****_ Es una
****** Necesidad logica <<tema6>>
*******_ Creada para
******** Cerrar ideas <<tema6>>
*********_ Compuesto
********** Por ningun elemento <<tema6>>
*** Representacion <<tema7>>
****_ Realizada mediante
***** Diagramas de Venn <<tema7>>
******_ Compuestos por
******* Circulos <<tema7>>
******* Ovalos <<tema7>>
******* Cualquier elemento que permita encerrar elementos <<tema7>>
******_ Creado por
******* Jhon Venn <<tema7>>
****** Facilitan Operaciones <<tema7>>
*******_ Por ser
******** Intuitivos <<tema7>>
****** No funcionan <<tema7>>
*******_ Para 
******** Demostrar resultados <<tema7>>
*** Cardinalidad de conjuntos <<tema8>>
****_ Es la cantidad de
***** Elementos presentes <<tema8>>
******_ En un 
******* Conjunto <<tema8>>
****_ Determinada por 
***** Una formula <<tema8>>
****** Cardinal del primer conjunto <<tema8>>
*******_ Sumado a la
******** Cardinal del segundo conjunto <<tema8>>
*********_ Restando al resultado
********** Cardinalidad de la interseccion de ambos conjuntos <<tema8>>
*** Acotacion de cardinales <<tema9>>
****_ Son relaciones de
***** Mayor <<tema9>>
***** Menor <<tema9>>
***** Igual <<tema9>>
*** Aplicaciones y funciones <<tema10>>
****_ Consiste en
***** Razon de cambio <<tema10>>
******_ Entre
******* Conjuntos <<tema10>>
********_ Que transforma a 
********* Un conjunto  <<tema10>>
**********_ En un 
*********** Elemento unico <<tema10>>
************_ De
************* Otro conjunto <<tema10>>
**** Tipos de aplicacion de conjuntos <<tema10>>
*****_ Son 
****** Aplicacion inyectiva <<tema10>>
****** Aplicacion Subyectiva <<tema10>>
****** Aplicacion Biyectiva <<tema10>>
**** Imagen <<tema10>>
*****_ Es
****** Conjunto formado <<tema10>>
*******_ Por
******** Todos los valores <<tema10>>
*********_ Que puede tomar una
********** Funcion <<tema10>>
**** Imagen inversa <<tema10>>
*****_ Consiste en
****** A cada subconjunto  <<tema10>>
*******_ del 
******** Conjunto final de la aplicacion <<tema10>>
*********_ le corresponde el
********** Conjunto de elementos <<tema10>>
***********_ Del
************ Conjunto inicial <<tema10>>
@endmindmap
```

Referencia bibliografica

[Temas 6 y 7: Conjuntos, Aplicaciones y funciones](https://canal.uned.es/video/5a6f1b77b1111f15098b4609)


# Funciones

```plantuml

@startmindmap
title Funciones
center footer Herrera Mendoza Juan Antonio
<style>
mindmapDiagram {
  .tema1 {
    BackgroundColor #FA6E50
  }
  .tema2{
      BackgroundColor #FA9250
  }
  .tema3 {
    BackgroundColor #FAAE50
  }
  .tema4 {
    BackgroundColor #FADB50
  }
  .tema5 {
    BackgroundColor #E7FA50
  }
  .tema6 {
    BackgroundColor #ABFA50
  }
  .tema7 {
    BackgroundColor #50FA6C
  }
  .tema8 {
    BackgroundColor #50FAA2
  }
  .tema9 {
    BackgroundColor #50FAD4
  }
  .tema10 {
    BackgroundColor #50E7FA
  }
    .tema11 {
    BackgroundColor #509FFA
  }
}
</style>

* Funciones <<tema1>>
** Matematicas <<tema2>>
***_ Surgen como
**** Intento de entendimiento <<tema2>>
*****_ Para
****** Resolver problemas cotidianos <<tema2>>
** Funcion <<tema3>>
***_ Es
**** Cambios <<tema3>>
*****_ En base a
****** Caracteristicas <<tema3>>
***_ Surgen de
**** Busqueda de comprension <<tema3>>
*****_ De
****** Entorno de forma matematica <<tema3>>
*******_ Mas que de
******** Forma intuitiva <<tema3>>
***_ Son una
**** Aplicacion especial <<tema3>>
*****_ De los
****** Conjuntos <<tema3>>
*******_ Siendo estos
******** Numeros reales <<tema3>>
*********_ Siguiendo 
********** Reglas que les producen un cambio <<tema3>>
** Conceptos <<tema4>>
*** Representacion grafica <<tema5>>
****_ Llamada
***** Cartesiana <<tema5>>
******_ En honor a
******* Rene Descartes <<tema5>>
******_ Es una representacion en
******* Un plano <<tema5>>
********_ Mediante
********* Ejes <<tema5>>
**********_ Siendo
*********** "Eje X" o "X" <<tema5>>
*********** "Eje y" o "F(X)" <<tema5>>
********_ A travez de un
********* Conjunto de puntos <<tema5>>
**********_ Que generan
*********** La grafica de una funcion <<tema5>>
*** Tipos de funciones <<tema6>>
****_ Existen
***** Crecientes <<tema6>>
******_ Si los
******* Valores  <<tema6>>
********_ De la
********* Variable dependiente aumentan <<tema6>>
***** Decrecientes <<tema6>>
******_ Si los
******* Valores  <<tema6>>
********_ De la
********* Variable dependiente disminuyen <<tema6>>
**** Lineales <<tema6>>
*****_ Son
****** Funciones <<tema6>>
*******_ Que siguen la formula
******** F(x)+b <<tema6>>
*********_ Representadas mediante
********** Una recta <<tema6>>
*** Intervalo <<tema7>>
****_ Es
***** Rango <<tema7>>
******_ De
******* Valores <<tema7>>
********_ Para
********* La funcion <<tema7>>
*** Maximos relativos <<tema8>>
****_ Consiste en el
***** Maximo valor <<tema8>>
******_ Dentro del
******* Rango <<tema8>>
********_ En el cual la
********* Funcion crece <<tema8>>
*** Minimos relativos <<tema9>>
****_ Consiste en el
***** Minimo valor <<tema9>>
******_ Dentro del
******* Rango <<tema9>>
********_ En el cual la
********* Funcion decrece <<tema9>>
*** Limite <<tema10>>
****_ Son
***** Valores <<tema10>>
******_ De la
******* Variable dependiente <<tema10>>
********_ Que en un
********* Punto <<tema10>>
**********_ Se acercan a
*********** Un valor <<tema10>>
****_ Una funcion
***** Carece de limite <<tema10>>
******_ Cuando se encuentra una
******* Discontinuidad <<tema10>>
********_ En un
********* Valor <<tema10>>
*** Derivada <<tema11>>
****_ Consiste en
***** Una aproximacion <<tema11>>
******_ De 
******* Una funcion complicada <<tema11>>
********_ A una
********* Funcion mas sencilla <<tema11>>
**********_ De
*********** Evaluacion <<tema11>>
****_ La 
***** Pendiente de la recta <<tema11>>
******_ Es lo que comunmente llamamos
******* Derivada <<tema11>>
****_ Existen
***** Formulas de derivadas <<tema11>>
******_ Como
******* Atajos <<tema11>>
********_ A la 
********* Resolucion de derivadas <<tema11>>

@endmindmap
```

Referencia bibliografica

[Funciones](https://canal.uned.es/video/5a6f2d09b1111f21778b457f)


# Las matemáticas de los computadores

```plantuml

@startmindmap
title Las matemáticas de los computadores
center footer Herrera Mendoza Juan Antonio
<style>
mindmapDiagram {
  .tema1 {
    BackgroundColor #F1699E
  }
  .tema2{
      BackgroundColor #F169DD
  }
  .tema3 {
    BackgroundColor #DA69F1
  }
  .tema4 {
    BackgroundColor #C469F1
  }
  .tema5 {
    BackgroundColor #9B69F1
  }
  .tema6 {
    BackgroundColor #6969F1
  }
  .tema7 {
    BackgroundColor #699BF1
  }
  .tema8 {
    BackgroundColor #69D3F1
  }
  .tema9 {
    BackgroundColor #69F1E7
  }
  .tema10 {
    BackgroundColor #69F1B0
  }
    .tema11 {
    BackgroundColor #69F176
  }
  }
  .tema12 {
    BackgroundColor #91F169
  }
    }
  .tema13 {
    BackgroundColor #BAF169
  }
    }
  .tema14 {
    BackgroundColor #DAF169
  }  
    }
  .tema15 {
    BackgroundColor #F1DA69
  }
}
</style>

* Las matemáticas de los computadores <<tema1>>
** Matematicas <<tema2>>
***_ Son los
**** Cimientos <<tema2>>
*****_ Del funcionamiento del
****** Computador <<tema2>>
*******_ Permitiendo realizar
******** Diferentes funciones <<tema2>>
** Aritmetica del computador <<tema3>>
***_ Surge para resolver 
**** La representacion <<tema4>>
*****_ de
****** Numeros reales <<tema4>>
*******_ En un 
******** Computador <<tema4>>
*********_ Debido a que existe un
********** Espacio reducido <<tema4>>
***********_ Derivado de
************ Posiciones en memoria <<tema4>>
*** Aspectos matematicos <<tema5>>
****_ Es una
***** Aritmetica finita <<tema6>>
****_ Uso de
***** Numeros Aproximados <<tema6>>
****_ Existencia de
***** Errores <<tema6>>
******_ Al aproximar un 
******* Numero real <<tema6>>
********_ Con un numero fijo de 
********* Cifras <<tema6>>
****_ Aparicion de
***** Digitos significativos <<tema6>>
******_ Son
******* Digitos <<tema6>>
********_ Que representan informacion sobre
********* La magnitud del signo <<tema6>>
**********_ Que acompaña al 
*********** Numero <<tema6>>
**** Truncamiento <<tema7>>
*****_ Consiste en
****** Cortar un numero <<tema7>>
*******_ Por el 
******** Lado derecho <<tema7>>
*********_ Que cuenta con
********** Infinitos decimales <<tema7>>
*****_ Esta operacion es
****** Dependiente  <<tema7>>
*******_ De las
******** Posiciones disponibles <<tema7>>
*********_ Con las que se cuenten 
********** En memoria <<tema7>>
*****_ Se considera 
****** Fiable <<tema7>>
*******_ Dependiendo del
******** Tipo de computo <<tema7>>
**** Redondeo <<tema8>>
*****_ Consiste en
****** Descartar cifras <<tema8>>
*******_ Al representar un 
******** Valor <<tema8>>
******_ Tambien llamado
******* Truncamiento refinado <<tema8>>
******_ Intenta evitar
******* Error en media <<tema8>>
********_ al
********* Alejarse del valor a representar <<tema8>>
*** Sistemas Numericos <<tema9>>
**** Sistema Binario <<tema10>>
***** Sistema de numeracion <<tema10>>
******_ Que utiliza
******* Base 2 <<tema10>>
***** Usos <<tema11>>
******_ Permite representar
******* Letras <<tema11>>
******* Signos especiales <<tema11>>
******* Numeros <<tema11>>
*****_ Aplicado a 
****** Circuitos electricos <<tema11>>
*******_ Usando sus valores para
******** Encendido <<tema11>>
******** Apagado <<tema11>>
****** Logica Booleana <<tema11>>
*****_ Permite mostrar mediante  
****** Bits <<tema12>>
*******_ Informacion como
******** Imagenes <<tema12>>
******** Videos <<tema12>>
******** Sonidos <<tema12>>
*****_ Gracias a la
****** Velocidad de procesamiento <<tema13>>
*******_ Permite construir 
******** Estructuras complejas <<tema13>>
****** Representacion interna de numeros <<tema13>>
*******_ Se realiza a traves de 
******** Bits <<tema13>>
*********_ Uniendo
********** Unos y ceros <<tema13>>
****** Tipos de representacion <<tema13>>
*******_ Los cuales son
******** Magnitud y signo <<tema13>>
******** Exceso <<tema13>>
******** Complemento 2 <<tema13>>
*******_ Muestran como convertir
******** Ceros y unos <<tema13>>
*********_ Almacenados en memoria en un
********** Numero entero <<tema13>>
****_ Tambien usados para compactar
***** Numeros con muchos digitos <<tema14>>
******_ Que se encuentren
******* En base 2 <<tema14>>
********_ Contamos con
********* Sistema Octal <<tema14>>
**********_ Usando
*********** Base 8 <<tema14>>
********* Sistema Hexadecimal <<tema14>>
**********_ Usando 
*********** Base 16 <<tema14>>
@endmindmap
```

Referencia bibliografica

[La matemática del computador. El examen final](https://canal.uned.es/video/5a6f1b76b1111f15098b45fa)

[Tema 12: Las matemáticas de los computadores. El examen final](https://canal.uned.es/video/5a6f1b81b1111f15098b4640)

